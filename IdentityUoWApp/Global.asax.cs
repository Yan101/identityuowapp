﻿using IdentityUoWApp.Models;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;

namespace IdentityUoWApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer<UserContext>(new UserDbInitializer());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
